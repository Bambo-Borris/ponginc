
#ifndef MATH_UTILS_H
#define MATH_UTILS_H

/// <summary>
/// Convert an angle from degrees to radians
/// </summary>
/// <param name="theta">Angle in degrees</param>
/// <returns>Angle in radians</returns>
float degrees_to_radians(float theta);

/// <summary>
/// Convert an angle in radians to degrees
/// </summary>
/// <param name="theta">Angle in radians</param>
/// <returns>Angle in degrees</returns>
float radians_to_degrees(float theta);

/// <summary>
/// Returns whichever of the two supplied numbers is largest
/// </summary>
/// <param name="first">First float to compare with</param>
/// <param name="second">Second float to compare with</param>
/// <returns>Maximum float from supplied params</returns>
float get_max_float(float first, float second);

/// <summary>
/// Returns whichever of the two supplied numbers is smallest
/// </summary>
/// <param name="first">First float to compare with</param>
/// <param name="second">Second float to compare with</param>
/// <returns>Minimum float from supplied params</returns>
float get_min_float(float first, float second);

/// <summary>
/// Clamp a value within a certain range
/// </summary>
/// <param name="min">Range minimum</param>
/// <param name="max">Range maximum</param>
/// <param name="value">Value to clamp</param>
/// <returns>Value which is clamped between min and max</returns>
float clamp_float(float min, float max, float value);


/// <summary>
/// Linear interpolation of a floating point value
/// </summary>
/// <param name="start">Start value</param>
/// <param name="end">End value</param>
/// <param name="t">Percentage along line</param>
/// <returns>Lerp'd value</returns>
float lerp_float(float start, float end, float t);
#endif // !MATH_UTILS_H
