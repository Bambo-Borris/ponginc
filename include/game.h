#ifndef GAME_H
#define GAME_H

#include <SFML\Graphics.h>
#include <stdbool.h>

#include "vector_utils.h"
#include "types.h"

typedef enum
{
	sMenu,
	sGamePlayAi,
	sGameplayNetwork,
	sCredits, 
	sFailScreen, 
	sWinScreen,
	MAX_APP_SATE
} ApplicationState;

typedef struct
{
	sfRectangleShape* shape;
	char id[20];
	vec2f position;

} paddle;

typedef struct
{
	sfCircleShape* shape;
	vec2f position;
	vec2f direction;
} ball;


extern paddle playerPaddle;
extern paddle aiPaddle;
extern ball gameBall;
extern ApplicationState appState;

const extern float PADDLE_WIDTH;
const extern float PADDLE_HEIGHT;
const extern float BALL_RADIUS;
const extern float PADDLE_MOVE_SPEED;
const extern float BALL_MOVE_SPEED;
const extern uint32 MAX_SCORE;
extern float speedMultiplier;

bool setup_game();

void handle_event(sfEvent* evt); 

void update_game(float dt);

void draw_game();

void cleanup_game();

static bool setup_paddles();

static bool setup_ball();

static bool setup_score_text();

static bool setup_dividing_line(); 

static bool setup_fail_screen();

static bool setup_win_screen();

static bool setup_border();

static bool load_sounds();

static void generate_random_direction();

static void update_score_text(int playerScore, int aiScore);

static void reset_game_ball();

static void update_state_gameplay(float dt);

static void update_state_game_end(float dt);

static void draw_state_gameplay(); 

static void draw_state_game_end();

static void reset_game();

#endif // !GAME_H
