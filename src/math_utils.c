#include "math_utils.h"
#define _USE_MATH_DEFINES
#include <math.h>

float degrees_to_radians(float theta)
{
	return (theta * (float)(M_PI)) / 180.0f;
}

float radians_to_degrees(float theta)
{
	return (theta * 180.0f) / (float)(M_PI);
}

float get_max_float(float first, float second)
{
	return first > second ? first : second;
}

float get_min_float(float first, float second)
{
	return first < second ? first : second;
}

float clamp_float(float min, float max, float value)
{
	if (value < min)
	{
		return min;
	}

	if (value > max)
	{
		return max;
	}

	return value;
}

float lerp_float(float start, float end, float t)
{
	return (1.0f - t) * start + t * end;
}
