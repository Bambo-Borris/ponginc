#include "application.h"

#include <SFML\System.h>
#include <assert.h>

#include "game.h"
#include "vector_utils.h"

sfRenderWindow* renderWindow = NULL;

static sfClock* delta_clock = NULL;
static sfColor clearCol;

const uint32 VIEW_WIDTH = 640;
const uint32 VIEW_HEIGHT = 480;

bool setup_application()
{
	sfVideoMode videoMode;
	videoMode.width = VIEW_WIDTH;
	videoMode.height = VIEW_HEIGHT;

	renderWindow = sfRenderWindow_create(videoMode, "Pong in C", sfDefaultStyle, NULL);
	if (!renderWindow)
	{
		return false;
	}

	sfView* view = sfView_copy(sfRenderWindow_getView(renderWindow));
	const vec2f ViewSize = { (float)(videoMode.width), (float)(videoMode.height) };
	sfView_setSize(view, ViewSize);
	sfRenderWindow_setView(renderWindow, view);
	sfRenderWindow_setKeyRepeatEnabled(renderWindow, true);
	clearCol = sfBlack;

	if (!setup_game(renderWindow))
	{
		return false;
	}

	return true;
}

void run_application()
{
	delta_clock = sfClock_create();

	while (sfRenderWindow_isOpen(renderWindow))
	{
		sfEvent e;
		const sfTime t = sfClock_restart(delta_clock);
		const float dt = sfTime_asSeconds(t);

		while (sfRenderWindow_pollEvent(renderWindow, &e))
		{
			if (e.type == sfEvtClosed)
			{
				sfRenderWindow_close(renderWindow);
			}

			if (e.type == sfEvtKeyPressed)
			{
				if (e.key.code == sfKeyEscape)
				{
					sfRenderWindow_close(renderWindow);
				}
			}
			handle_event(&e);
		}

		update_game(dt);

		sfRenderWindow_clear(renderWindow, clearCol);
		draw_game();
		sfRenderWindow_display(renderWindow);
	}
}

void cleanup_application()
{
	sfRenderWindow_destroy(renderWindow);
	renderWindow = NULL;
}

void set_clear_colour(sfColor* col)
{
	assert(col);
	clearCol = *col;
}
