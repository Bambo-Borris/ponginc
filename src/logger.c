#include "logger.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <SFML\System\Mutex.h>

#include "types.h"

static LoggerLevel lLevel = lTrace;
static bool firstOpen = true;
static sfMutex* loggerMutex = NULL;

void set_logger_level(LoggerLevel l)
{
	lLevel = l;
}

void log_trace(const char* fmt, ...)
{
	if ((lLevel > lTrace))
	{
		return;
	}
	va_list valist;
	va_start(valist, fmt);
	print("[Trace]", fmt, valist);
	va_end(valist);
}

void log_err(const char* fmt, ...)
{
	if ((lLevel > lError))
	{
		return;
	}
	va_list valist;
	va_start(valist, fmt);
	print("[Error]", fmt, valist);
	va_end(valist);
}

void log_dbg(const char* fmt, ...)
{
	if ((lLevel > lDebug))
	{
		return;
	}
	va_list valist;
	va_start(valist, fmt);
	print("[Debug]", fmt, valist);
	va_end(valist);
}

void print(const char* outputHeader, const char* fmt, va_list args)
{
	static char finalBuff[120];
	static char fmtBuff[100];
	static const uint64 FmtBuffSize = sizeof(fmtBuff) / sizeof(char);
	static const uint64 FinalBuffSize = sizeof(finalBuff) / sizeof(char);

	if (!loggerMutex)
	{
		loggerMutex = sfMutex_create();
		assert(loggerMutex);
	}

	sfMutex_lock(loggerMutex);
	memset(fmtBuff, 0, FmtBuffSize);
	memset(finalBuff, 0, FinalBuffSize);

	//sprintf_s(str, BuffSize, valist);
	vsprintf_s(fmtBuff, FmtBuffSize, fmt, args);
	sprintf_s(finalBuff, FinalBuffSize, "%s %s", outputHeader, fmtBuff);
	printf_s("%s\n", finalBuff);

	FILE* logFile = NULL;
	if (firstOpen)
	{
		if (fopen_s(&logFile, "log.txt", "w+"))
		{
			printf_s("[Error] Unable to create log file!\n");
			sfMutex_unlock(loggerMutex);
			return;
		}
		firstOpen = false;
	}
	else
	{
		if (fopen_s(&logFile, "log.txt", "a+"))
		{
			printf_s("[Error] Unable to write to log file!\n");
			sfMutex_unlock(loggerMutex);
			return;
		}
	}

	fwrite(finalBuff, 1, strnlen_s(finalBuff, FinalBuffSize), logFile);
	fwrite("\n", 1, 1, logFile);
	fclose(logFile);
	sfMutex_unlock(loggerMutex);
}
