#ifndef LOGGER_H
#define LOGGER_H

#include <stdarg.h>

typedef enum
{
	lDebug = 1 << 0,
	lTrace = 1 << 1,
	lError = 1 << 2
} LoggerLevel;

void set_logger_level(LoggerLevel l);

void log_trace(const char* fmt, ...);
void log_err(const char* fmt, ...);
void log_dbg(const char* fmt, ...);

static void print(const char* outputHeader, const char* fmt, va_list args);
#endif // !LOGGER_H
