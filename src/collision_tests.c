#include "collision_tests.h"

#include <assert.h>
#include <string.h>
#include <math.h>

#include "math_utils.h"
bool aabb_vs_aabb(sfRectangleShape* rectA, sfRectangleShape* rectB, collision_data* result)
{
	return false;
}

bool circle_vs_circle(sfCircleShape* circleA, sfCircleShape* circleB, collision_data* result)
{
	assert(circleA);
	assert(circleB);
	assert(result);

	memset(result, 0, sizeof(collision_data));

	sfTransform transformA = sfCircleShape_getTransform(circleA);
	const float RadiusA = sfCircleShape_getRadius(circleA);
	const vec2f originA = { RadiusA, RadiusA };
	const vec2f centreA = sfTransform_transformPoint(&transformA, originA);

	sfTransform transformB = sfCircleShape_getTransform(circleB);
	const float RadiusB = sfCircleShape_getRadius(circleB);
	const vec2f originB = { RadiusB, RadiusB };
	const vec2f centreB = sfTransform_transformPoint(&transformB, originB);
	vec2f pos = sfCircleShape_getPosition(circleB);

	const vec2f Direction = sub_vec2f(&centreB, &centreA);
	const float DistSq = get_square_length_vec2f(&Direction);

	float RadiusDistSquare = powf(RadiusA + RadiusB, 2.0f);

	if (DistSq > RadiusDistSquare)
	{
		return false;
	}

	const float Length = sqrtf(DistSq);

	if (Length != 0.0f)
	{
		result->penetration = sqrtf(RadiusDistSquare) - Length;
		result->normal = divide_vec2f_float(&Direction, Length);
	}
	else
	{
		result->penetration = RadiusA;
		result->normal.x = 1.0f;
		result->normal.y = 0.0f;
	}
	return true;
}

bool aabb_vs_circle(sfRectangleShape* rect, sfCircleShape* circle, collision_data* result)
{
	assert(rect);
	assert(circle);
	assert(result);

	const vec2f RectSize = sfRectangleShape_getSize(rect);
	const vec2f HalfRectSize = divide_vec2f_float(&RectSize, 2.0f);
	sfTransform rectTransform = sfRectangleShape_getTransform(rect);
	const vec2f RectCentre = sfTransform_transformPoint(&rectTransform, divide_vec2f_float(&RectSize, 2.0f));

	sfTransform circleTransform = sfCircleShape_getTransform(circle);
	const float Radius = sfCircleShape_getRadius(circle);
	const vec2f CircleOrigin = { Radius, Radius };
	const vec2f CircleCentre = sfTransform_transformPoint(&circleTransform, CircleOrigin);

	vec2f direction = sub_vec2f(&CircleCentre, &RectCentre);
	vec2f closestPoint = direction;

	float xExtent = ((RectCentre.x + HalfRectSize.x) - (RectCentre.x - HalfRectSize.x)) / 2.0f;
	float yExtent = ((RectCentre.y + HalfRectSize.y) - (RectCentre.y - HalfRectSize.y)) / 2.0f;

	closestPoint.x = clamp_float(-xExtent, xExtent, closestPoint.x);
	closestPoint.y = clamp_float(-yExtent, yExtent, closestPoint.y);

	bool inside = false;

	if (closestPoint.x == direction.x && closestPoint.y == direction.y)
	{
		inside = true;
		if (fabs(direction.x) > fabs(direction.y))
		{
			if (closestPoint.x > 0.0f)
			{
				closestPoint.x = xExtent;
			}
			else
			{
				closestPoint.x = -xExtent;
			}
		}
		else
		{
			if (closestPoint.y > 0.0f)
			{
				closestPoint.y = yExtent;
			}
			else
			{
				closestPoint.y = -yExtent;
			}
		}
	}

	vec2f normal = sub_vec2f(&direction, &closestPoint);

	const float DistSq = get_square_length_vec2f(&normal);

	if (DistSq > (Radius * Radius) && !inside)
	{
		return false;
	}

	float Dist = sqrtf(DistSq);
	normal = divide_vec2f_float(&normal, Dist);

	if (inside)
	{
		result->normal = multiply_vec2f_float(&normal, -1.0f);
	}
	else
	{
		result->normal = normal;
	}

	result->penetration = Radius - Dist;
	return true;
}
