#ifndef MENU_GUI_H
#define MENU_GUI_H

#include <stdbool.h>
#include <SFML\Graphics\Font.h> 
#include <SFML\Graphics\Color.h>
#include <SFML\Window\Event.h>

#include "types.h"

typedef enum
{
	mMainOptions, 
	mPlayOptions, 
	mMultiplayerOptions,
	mHostGameOptions,
	mJoinGameOptions,
	mCredits
} MenuState;

bool setup_menu_state(sfFont* font); 

void update_menu_state(float dt, void (*reset_ptr) ());

void handle_menu_events(sfEvent* evt); 

void draw_menu_state();

void cleanup_menu();

void return_to_menu();

static void update_main_options(float dt, vec2f* mousePos);

static void update_play_options(float dt, vec2f* mousePos, void (*reset_ptr) ());

static void update_multiplayer_options(float dt, vec2f* mousePos);

static sfColor get_hover_col(sfColor* col);

#endif // !MENU_GUI_H
