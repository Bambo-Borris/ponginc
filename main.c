#include <stdio.h>
#include <SFML\Graphics.h>
#include <SFML\System.h>
#include <assert.h>

#include "vector_utils.h"
#include "application.h"
#include "logger.h"

int main(int argc, char* argv[])
{
	bool didCreate = setup_application(); 
	if (!didCreate)
	{
		log_err("Failed to setup application!");
		return 1;
	}

#ifdef _DEBUG
	set_logger_level(lDebug);
#else
	set_logger_level(lError); 
#endif

	run_application();
	cleanup_application();
	return 0;
}