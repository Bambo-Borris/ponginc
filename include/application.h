#ifndef APPLICATION_H
#define APPLICATION_H

#include <stdbool.h>
#include <SFML\Graphics.h>
#include "types.h"

extern sfRenderWindow* renderWindow;
const extern uint32 VIEW_WIDTH;
const extern uint32 VIEW_HEIGHT;

bool setup_application(); 
void run_application(); 
void cleanup_application(); 
void set_clear_colour(sfColor* col);

#endif // !APPLICATION_H
